import React, {Fragment, useState, useEffect} from 'react';
import Navbar from './components/layouts/Navbar';
import {
  BrowserRouter as Router,
  Switch,
  Route
}
  from "react-router-dom";
import Footer from './components/layouts/Footer';
import Home from './pages/Home';
import About from './pages/About';
import Gallery from './pages/Gallery';
import ContactUs from './pages/Contact';
import Login from './components/forms/Login';
import Register from './components/forms/Register';
import AddRoom from './components/forms/AddRoom';
import jwt_decode from 'jwt-decode';
import Auth from './Auth';
import AllRooms from './pages/rooms/AllRooms';
import Couple from './pages/rooms/Couple';
import King from './pages/rooms/King';
import RoomDetails from './pages/rooms/RoomDetails';
import Queen from './pages/rooms/Queen';
import Family from './pages/rooms/Family';
import Transaction from './pages/Transaction';
import YourBooking from './pages/YourBooking';
import CheckOut from './pages/CheckOut';
import Message from './pages/Message';
import UserTransaction from './pages/UserTransaction';
import SimpleReactLightbox from 'simple-react-lightbox'
import CoupleRoom from './pages/room-galleries/coupleroom';
import FamilyRoom from './pages/room-galleries/familyroom';
import KingRoom from './pages/room-galleries/kingroom';
import QueenRoom from './pages/room-galleries/queenroom';
import Food from './pages/room-galleries/food';
import Activity from './pages/room-galleries/actvity';
import { BASE_URL } from './config'


function App() {
  const [rooms, setRooms] = useState([])
  const [user, setUser] = useState([])
  const [token, setToken] = useState("")

  useEffect(() => {
    fetch(`${BASE_URL}/rooms`)
      .then(res => res.json())
      .then(data => {
      setRooms(data)
      })
    if (token) {
      let decoded = jwt_decode(token)
      let now = new Date()
      if (decoded.exp === now.getTime()) {
        localStorage.clear()
        window.location.href="/"
      }
    }
    setUser(JSON.parse(localStorage.getItem("user")))
    setToken(localStorage.getItem("token"))
  }, [rooms])

  const logoutHandler = () => {
    localStorage.clear()
    setUser({})
    setToken("")
    window.location.href="/"
  }
  return (
      <Router>
      <Fragment>
        <Navbar user={user} token={token} logoutHandler={logoutHandler} />
       
        <Switch>
          <Route exact path="/">
            <Home/>
          </Route>

          <Route path="/contact-us">
            <ContactUs/>
          </Route>

          <Route path="/messages">
          { user && token && user.isAdmin ?
            <Message token={token} />
            : <Auth />}
          </Route> 

          <Route path="/about">
            <About/>
          </Route>
          
          <Route path="/login">
            <Login/>
          </Route>

          <Route path="/register">
            <Register/>
          </Route>

          {/*<Route path="/all-rooms">
            <AllRoom token={token} user={user}/>
            </Route>*/}
            
          
          <Route path="/all-rooms">
            <AllRooms/>
          </Route>

        <Route path="/your-booking">
          {user && token && user.isAdmin === false ? <YourBooking token={token} user={user} /> : <Auth/> }
          </Route>

          <Route path="/transactions/:userId">
          {user && token && user.isAdmin === false ? <UserTransaction /> : <Auth/> }
        </Route>
        <Route path="/transactions">
        { user && token && user.isAdmin ?
            <Transaction />
            : <Auth />}
        </Route>
          
          <Route path="/booking-details">
            <CheckOut user={user}/>
          </Route>

          <Route path="/couple">
            <Couple rooms={rooms} user={user} token={token}/>
          </Route>

          <Route path="/family">
            <Family rooms={rooms} user={user} token={token}/>
          </Route>

          
          <Route path="/king">
            <King rooms={rooms} user={user} token={token}/>
          </Route>

          <Route path="/queen">
            <Queen rooms={rooms} user={user} token={token}/>
          </Route>

          <Route exact path="/gallery">
            <Gallery/>
            </Route>

          <Route path="/rooms/:id">
            <RoomDetails user={user} token={token} rooms={rooms}/> 
          </Route>
          <Route path="/add-room">
          {user && token && user.isAdmin ? <AddRoom/> :<Auth/> }
          </Route>
          
          <SimpleReactLightbox>
            <Route path="/gallery/coupleroom">
              <CoupleRoom/>
            </Route>

            <Route path="/gallery/familyroom">
              <FamilyRoom/>
            </Route>

            <Route path="/gallery/kingroom">
              <KingRoom/>
            </Route>

            <Route path="/gallery/queenroom">
              <QueenRoom/>
            </Route>

            <Route path="/gallery/foods">
              <Food/>
            </Route>

            <Route path="/gallery/activities">
              <Activity/>
            </Route>

        </SimpleReactLightbox>
        
        </Switch>
        <Footer/>
      </Fragment>
      
      </Router>
  )
}

export default App;

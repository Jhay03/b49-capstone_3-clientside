import React from 'react';
import {Link} from 'react-router-dom'
import { BASE_URL } from '../config'

const Room = ({ rooms, user, token }) => {
    const style = {
        "height": "50vh",
        "width": "100%"
    }

    const showRooms = rooms.map(room => (
                <div className="col col-lg-3" key={room._id}>
                    <div className="card p-2 h-100 mb-2">
                            <img src={`${BASE_URL}/${room.image}`} alt="roomImg" className="img-fluid" style={style} />
                        <div className="card-body">
                           <Link to={`/rooms/${room._id}`}>
                            <h3 className="text-center">{room.roomname}</h3></Link>
                        </div>
                        <div className="card-title">   
                            <p>{room.description}</p>
                            <h6>{room.price}</h6>
                        </div>  
                        <button className="btn btn-outline-primary">Book now</button>
                    </div>
                
                    
                </div>
    ))
    return (
        <div className="container">
            <div className="text-center">
                <h2> Book a Room now!</h2>
            </div>
            <p>
                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ipsum odio praesentium numquam ullam consectetur vitae repellat, dignissimos suscipit sequi libero?
            </p>
            <div className=" row">
            {showRooms}

            </div>
        </div>
    )
}

export default Room;
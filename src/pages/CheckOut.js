import React, {useState, useEffect, Fragment,Component } from 'react';
import error403 from '../images/403.png'
import jsPDF from 'jspdf'
import {Link} from 'react-router-dom'
import { BASE_URL } from '../config'
import html2canvas from 'html2canvas';
import $ from 'jquery'
const CheckOut = ({id, user}) => {
    const [transactions, setTransactions] = useState([])
    useEffect(() => {
        fetch(`${BASE_URL}/transactions`)
        .then(res => res.json())
            .then(data => {
                setTransactions(data)
            })
    }, [])
    const jsPdfGenerator = () => {
        const page = document.getElementById('test')
                    
        html2canvas(page)
        .then((canvas) => {
            const imgData = canvas.toDataURL('image/png');
            const pdf = new jsPDF('p', 'px', 'a4');
            const width = pdf.internal.pageSize.getWidth();
            const height = pdf.internal.pageSize.getHeight();

            pdf.addImage(imgData, 'JPEG', 0, 0, width, height);
            pdf.save("RAM-Receipt.pdf");
        });
    }
    let kahitano = transactions.length - 1;
    /* console.log(transactions[kahitano]) */
     const showBookingDetails = transactions.filter(filterTrans => filterTrans._id === transactions[kahitano]._id).map(transaction => (
            
          <div className="row">
            <div className="col-md-10 col-sm-8">
                <div className="checkout-container">
                    <div className="card checkout-card">
                        <h3 className="italicBoldText mb-3">Booking Successful</h3>
                            <div className="row">
                                
                                <div className="col-md-5">
                                        <h3 className="cyanText">Transaction Details</h3>
                                        
                                        <p>Your transaction # is : {transaction._id}</p>
                        
                                        <p>Book Status : {transaction.statusId == "5e9eb2e60cf7a34fb863b113" ?
                                                "Reserved" : (transaction.statusId == "5e9eb2eb0cf7a34fb863b114" ?
                                                "Paid" : "Cancelled"
                                                )}
                                        </p>
                                        <p>Payment Mode : {transaction.paymentMode}</p>
                                        <p>Total : {transaction.total}</p>
                                </div>
                                
                                
                                <div className="col-md-5">
                                        <h3 className="cyanText">Room Details</h3>
                                        {transaction.rooms.map(room => (
                                    <Fragment>
                                            <p> Booking # : {room.bookingId}</p>
                                            <p> Room name : {room.roomname}</p>
                                            <p> Price Per Night : {room.price}</p>
                                            <p> Check In Date : {new Date(room.checkIn).toLocaleString()}</p>
                                            <p> Check Out Date : {new Date(room.checkOut).toLocaleString()}</p>
                                            <p> Number of days :{room.noOfDays} </p>
                                    </Fragment>
                                    ))}
                                </div>
                            </div>
                            <div>
                            <button className="declineMore btn" onClick={jsPdfGenerator}>Print Receipt</button>
                            <button className="cyanButton btn ml-3"><Link to={`/transactions/${user._id}`} className="cyanButton btn ml-3">Check your transaction</Link></button>
                            </div>
                        </div>
                    </div>   
                </div>
        </div> 
    ))
 
    return (
        <div className="image-container set-full-height" id="test">
            {showBookingDetails}
        </div>
    )
}


export default CheckOut;
import React from 'react';
import aboutUs from '../images/aboutUs.jpg'
import '../App.css'
import Jhay from '../images/Jhay.jpg'
const About = () => {
    return (
   <div className="container">
        <div className="about-container">
            <img src={aboutUs} className="imageBanner" alt="ïmageBanner"/>
                <h2 className="text-center mt-3">About Us </h2>
                {/*<img src={Logo} alt="logo" className="logo" />*/}

        <div className="content">
            <p>
                    Despite having moved to the United States, we always considered Obando as our second hometown. Having experienced deluxe and elegant accommodations all over the world, it had been frustrating for us to find no such 
                    place in Obando to privately gather and spend time with family and friends every time we go home.        
            </p>
                
            <p>
                    Despite having moved to the United States, we always considered Obando as our second hometown. Having experienced deluxe and elegant accommodations all over the world, it had been frustrating for us to find no such 
                    place in Obando to privately gather and spend time with family and friends every time we go home.        
            </p>
                
            <p>
                    Despite having moved to the United States, we always considered Obando as our second hometown. Having experienced deluxe and elegant accommodations all over the world, it had been frustrating for us to find no such 
                    place in Obando to privately gather and spend time with family and friends every time we go home.        
            </p>   
        </div>
        
        <div className="management-container">
                <h2 className="text-center">Our Management Team</h2>
                
                <div className="card-deck">
                   
                    <div className="col-lg-4 col-sm-12">
                        <div className="card">
                                <img src={Jhay} alt="first" className="img-fluid" />
                                <h6 className="text-center">Romeo Marquez</h6>
                            <div className="card-body">
                                <p className="omtPara">
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                        Sit esse numquam architecto assumenda quas id nam tenetur a eaque ad!
                                </p>
                            </div>
                        </div>
                    </div>
                    
                    <div className="col-lg-4 col-sm-12">
                        <div className="card">
                                <img src={Jhay} alt="first" className="img-fluid" />
                                <h6 className="text-center">Romeo Marquez</h6>
                            <div className="card-body">
                                <p className="omtPara">
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                        Sit esse numquam architecto assumenda quas id nam tenetur a eaque ad!
                                </p>
                            </div>
                        </div>
                    </div>


                    <div className="col-lg-4 col-sm-12">
                        <div className="card">
                                <img src={Jhay} alt="first" className="img-fluid aboutImg" />
                                <h6 className="text-center">Chopper Marquez</h6>
                            <div className="card-body">
                                <p className="omtPara">
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                        Sit esse numquam architecto assumenda quas id nam tenetur a eaque ad!
                                </p>
                            </div>
                        </div>
                    </div>    
                    
                </div>    
        </div>
    </div>
   </div>
    )
}

export default About;
import React from 'react';
import { SRLWrapper } from 'simple-react-lightbox';
import kingRoom from '../../images/King/kingBanner.jpg'
import kingRoom1 from '../../images/King/kingRoom1.jpg'
import kingRoom2 from '../../images/King/kingRoom2.jpg'
import kingRoom3 from '../../images/King/kingRoom3.jpg'
import kingRoom4 from '../../images/King/kingRoom4.jpg'
import {Link} from 'react-router-dom'

const KingRoom = () => {
    return (
        <div className="container">
            <Link to="/gallery" className="viewMore btn mt-2">Back to Gallery</Link>
            <h1 className="cyanText text-center">King Size Bedroom</h1>
             <img src={kingRoom} alt="banner" className="first img-fluid img-thumbnail mb-2"/>
            <SRLWrapper>
                    <img src={kingRoom1} alt="Room 1" className="test1 img-fluid img-thumbnail"/>
                    <img src={kingRoom2} alt="Room 2" className="test1 img-fluid img-thumbnail" />
                    <img src={kingRoom3} alt="Room 3" className="test1 img-fluid img-thumbnail"/>
                    <img src={kingRoom4} alt="Room 4" className="test1 img-fluid img-thumbnail "/>
            </SRLWrapper>
        </div>
    )
}

export default KingRoom;
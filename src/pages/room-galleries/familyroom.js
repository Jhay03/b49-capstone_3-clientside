import React from 'react';
import { SRLWrapper } from 'simple-react-lightbox';
import familyRoom from '../../images/Family/familyBanner.jpg'
import familyRoom1 from '../../images/Family/familyRoom1.jpg'
import familyRoom2 from '../../images/Family/familyRoom2.jpg'
import familyRoom3 from '../../images/Family/familyRoom3.jpg'
import familyRoom4 from '../../images/Family/familyRoom4.jpg'
import {Link} from 'react-router-dom'
const FamilyRoom = () => {
    return (
        <div className="container">
            <Link to="/gallery" className="viewMore btn mt-2">Back to Gallery</Link>
            <h1 className="cyanText text-center">Family Room</h1>
             <img src={familyRoom} alt="banner" className="first img-fluid img-thumbnail mb-2"/>
            <SRLWrapper>
                    <img src={familyRoom1} alt="Room 1" className="test1 img-fluid img-thumbnail"/>
                    <img src={familyRoom2} alt="Room 2" className="test1 img-fluid img-thumbnail" />
                    <img src={familyRoom3} alt="Room 3" className="test1 img-fluid img-thumbnail"/>
                    <img src={familyRoom4} alt="Room 4" className="test1 img-fluid img-thumbnail "/>
            </SRLWrapper>
        </div>
    )
}

export default FamilyRoom;
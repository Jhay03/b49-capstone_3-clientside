import React from 'react';
import { SRLWrapper } from 'simple-react-lightbox';
import queenRoom from '../../images/Queen/queenBanner.jpg'
import queenRoom1 from '../../images/Queen/queenRoom1.jpg'
import queenRoom2 from '../../images/Queen/queenRoom2.jpg'
import queenRoom3 from '../../images/Queen/queenRoom3.jpg'
import queenRoom4 from '../../images/Queen/queenRoom4.jpg'
import {Link} from 'react-router-dom'

const QueenRoom = () => {
    return (
        <div className="container">
            <Link to="/gallery" className="viewMore btn mt-2">Back to Gallery</Link>
            <h1 className="cyanText text-center">Queen Size Bedroom</h1>
             <img src={queenRoom} alt="banner" className="first img-fluid img-thumbnail mb-2"/>
            <SRLWrapper>
                    <img src={queenRoom1} alt="Room 1" className="test1 img-fluid img-thumbnail"/>
                    <img src={queenRoom2} alt="Room 2" className="test1 img-fluid img-thumbnail" />
                    <img src={queenRoom3} alt="Room 3" className="test1 img-fluid img-thumbnail"/>
                    <img src={queenRoom4} alt="Room 4" className="test1 img-fluid img-thumbnail "/>
            </SRLWrapper>
        </div>
    )
}

export default QueenRoom;
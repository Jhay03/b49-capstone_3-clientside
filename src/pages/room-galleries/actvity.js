import React from 'react';
import { SRLWrapper } from 'simple-react-lightbox';
import activityMain from '../../images/Activity/activityBanner.jpg'
import activityPic1 from '../../images/Activity/activityPic1.jpg'
import activityPic2 from '../../images/Activity/activityPic2.jpg'
import activityPic3 from '../../images/Activity/activityPic3.jpg'
import activityPic4 from '../../images/Activity/activityPic4.jpg'
import {Link} from 'react-router-dom'

const Activity = () => {
    return (
        <div className="container">
            <Link to="/gallery" className="viewMore btn mt-2">Back to Gallery</Link>
            <h1 className="cyanText text-center">Activity Section</h1>
             <img src={activityMain} alt="banner" className="first img-fluid img-thumbnail mb-2"/>
            <SRLWrapper>
                    <img src={activityPic1} alt="Parasailing" className="test1 img-fluid img-thumbnail"/>
                    <img src={activityPic2} alt="Snorkeling" className="test1 img-fluid img-thumbnail" />
                    <img src={activityPic3} alt="Banana Boat" className="test1 img-fluid img-thumbnail"/>
                    <img src={activityPic4} alt="Jetski" className="test1 img-fluid img-thumbnail "/>
            </SRLWrapper>

        </div>
    )
}

export default Activity;
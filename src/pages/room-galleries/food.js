import React from 'react';
import { SRLWrapper } from 'simple-react-lightbox';
import foodMain from '../../images/Food/foodBanner.jpg'
import foodPic1 from '../../images/Food/foodPic1.jpg'
import foodPic2 from '../../images/Food/foodPic2.jpg'
import foodPic3 from '../../images/Food/foodPic3.jpg'
import foodPic4 from '../../images/Food/foodPic4.jpg'
import {Link} from 'react-router-dom'

const Food = () => {
    return (
        <div className="container">
            <Link to="/gallery" className="viewMore btn mt-2">Back to Gallery</Link>
            <h1 className="cyanText text-center">Food Section</h1>
             <img src={foodMain} alt="banner" className="first img-fluid img-thumbnail mb-2"/>
            <SRLWrapper>
                    <img src={foodPic1} alt="Food 1" className="test1 img-fluid img-thumbnail"/>
                    <img src={foodPic2} alt="Food 2" className="test1 img-fluid img-thumbnail" />
                    <img src={foodPic3} alt="Food 3" className="test1 img-fluid img-thumbnail"/>
                    <img src={foodPic4} alt="Food 4" className="test1 img-fluid img-thumbnail "/>
            </SRLWrapper>

        </div>
    )
}

export default Food;
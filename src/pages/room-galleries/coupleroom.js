import React from 'react';
import { SRLWrapper } from 'simple-react-lightbox';
import coupleRoom from '../../images/Couple/coupleBanner.jpg'
import coupleRoom1 from '../../images/Couple/coupleRoom1.jpg'
import coupleRoom2 from '../../images/Couple/coupleRoom2.jpg'
import coupleRoom3 from '../../images/Couple/coupleRoom3.jpg'
import coupleRoom4 from '../../images/Couple/coupleRoom4.jpg'
import {Link} from 'react-router-dom'

const CoupleRoom = () => {
    return (
        <div className="container">
            <Link to="/gallery" className="viewMore btn mt-2">Back to Gallery</Link>
            <h1 className="cyanText text-center">Couple Room</h1>
             <img src={coupleRoom} alt="banner" className="first img-fluid img-thumbnail mb-2"/>
            <SRLWrapper>
                    <img src={coupleRoom1} alt="Room 1" className="test1 img-fluid img-thumbnail"/>
                    <img src={coupleRoom2} alt="Room 2" className="test1 img-fluid img-thumbnail" />
                    <img src={coupleRoom3} alt="Room 3" className="test1 img-fluid img-thumbnail"/>
                    <img src={coupleRoom4} alt="Room 4" className="test1 img-fluid img-thumbnail "/>
            </SRLWrapper>
        </div>
    )
}

export default CoupleRoom;
import React, {useState, useEffect, Fragment} from 'react';
import { BASE_URL } from '../config'

const Transaction = () => {
    const italic = {
        "fontStyle": "italic",
        "color": "gray"
    }
    const [transactions, setTransactions] = useState([])
    useEffect(() => {
        fetch(`${BASE_URL}/transactions`, {
            headers: {
                "x-auth-token": localStorage.getItem('token')
            }
        })
        .then(res => res.json())
        .then(data => setTransactions(data))
    })

    const updateStatus = (statusId, transactionId) => {
        let body = {
            statusId
        }
        fetch(`${BASE_URL}/transactions/${transactionId}`, {
            method: "PUT",
            body: JSON.stringify(body),
            headers: {
                "Content-Type": "application/json",
                "x-auth-token": localStorage.getItem('token')
            }
        })
            .then(res => res.json())
            .then(data => {
            alert(data.message)
        })
    }

    const validateDate = (createdDate) => {        
        let getStartDate = new Date(createdDate).getTime(); 
        let getEndDate = new Date().getTime();
        let differenceInTime = (getEndDate - getStartDate);
        let differenceInDay = Math.ceil(differenceInTime / (1000 * 3600 * 24));
        return (differenceInDay <3 )? true:false ;
    }
    return (
        <div className="container">
        <h2 className="cyanText text-center"> Transaction History</h2>
        {
            transactions.length ?
            <table className="table table-hover">
                <thead className="thead-dark">
                        <tr>
                            <th>Transaction ID</th>
                            <th>Status</th>
                            <th>Amount</th>
                            <th>Action(s)</th>
                        </tr>   
                </thead>
                <tbody>
                        {transactions.map(transaction => (
                        <tr key={transaction._id}>
                            <td>{transaction._id}</td>
                                <td>
                                {transaction.statusId == "5e9eb2e60cf7a34fb863b113" ?
                                "Reserved" : (transaction.statusId == "5e9eb2eb0cf7a34fb863b114" ?
                                "Paid" : "Cancelled"
                                )
                            }
                                </td>
                                <td>PHP : {transaction.total}</td>
                                {transaction.statusId === "5e9eb2e60cf7a34fb863b113" ? 
                            <td>
                                {validateDate(new Date(transaction.dateCreated).toLocaleString()) === false ?
                                <Fragment>
                                    <button className="cancelButton btn disabled mr-2">Approve</button>
                                    <button className="cancelButton btn disabled mr-2">Decline</button>
                                    <button className="cyanButton btn mr-2"
                                    onClick={() => updateStatus("5e9eb2f00cf7a34fb863b115", transaction._id)}
                                    >Cancel Booking</button>
                                </Fragment>
                                :
                                <Fragment>
                            <button className="viewMore btn mr-2"
                                    onClick={() => updateStatus("5e9eb2eb0cf7a34fb863b114", transaction._id)}
                                        >
                                    Approve
                            </button>
                            
                            <button className="declineMore btn"
                                    onClick={() => updateStatus("5e9eb2f00cf7a34fb863b115", transaction._id)}
                                        >
                                Decline
                            </button>
                            </Fragment>}
                            </td> : null
                             }    
                        </tr>
                    ))}        
                </tbody>
            </table>    
            : <h2 className="cyanText text-center"> No transactions to show. </h2>
            }
            <div className="col-md-12 col-sm-12">
            {
            transactions.length ?
            <table className="table table-responsive">
                <thead className="thead-dark">
                        <tr>
                            <th>Transaction ID</th>
                            <th>Status</th>
                            <th>Amount</th>
                            <th>Action(s)</th>
                        </tr>   
                </thead>
                <tbody>
                        {transactions.map(transaction => (
                        <tr key={transaction._id}>
                            <td>{transaction._id}</td>
                                <td>
                                {transaction.statusId == "5e9eb2e60cf7a34fb863b113" ?
                                "Reserved" : (transaction.statusId == "5e9eb2eb0cf7a34fb863b114" ?
                                "Paid" : "Cancelled"
                                )
                            }
                                </td>
                                <td>PHP : {transaction.total}</td>
                                {transaction.statusId === "5e9eb2e60cf7a34fb863b113" ? 
                            <td>
                                {validateDate(new Date(transaction.dateCreated).toLocaleString()) === false ?
                                <Fragment>
                                    <button className="cancelButton btn disabled mr-2">Approve</button>
                                    <button className="cancelButton btn disabled mr-2">Decline</button>
                                    <button className="cyanButton btn mr-2"
                                    onClick={() => updateStatus("5e9eb2f00cf7a34fb863b115", transaction._id)}
                                    >Cancel Booking</button>
                                </Fragment>
                                :
                                <Fragment>
                            <button className="viewMore btn mr-2"
                                    onClick={() => updateStatus("5e9eb2eb0cf7a34fb863b114", transaction._id)}
                                        >
                                    Approve
                            </button>
                            
                            <button className="declineMore btn"
                                    onClick={() => updateStatus("5e9eb2f00cf7a34fb863b115", transaction._id)}
                                        >
                                Decline
                            </button>
                            </Fragment>}
                            </td> : null
                             }    
                        </tr>
                    ))}        
                </tbody>
            </table>   
            :null} 
            </div>
           
        <p style={italic}>This Transaction table was created by <a href="https://www.facebook.com/oreomeo25" target="_blank">Romeo Marquez</a></p>
    </div>
    )
}

export default Transaction;
import React, {useState, useEffect, Fragment} from 'react';
import { useParams } from 'react-router-dom'
import { BASE_URL } from '../config'

const UserTransaction = () => {
    let { userId } = useParams()
    const [transactions, setTransactions] = useState([])
    const italic = {
        "fontStyle": "italic",
        "color": "gray"
    }
    useEffect(() => {
        fetch(`${BASE_URL}/transactions/${userId}`, {
            headers: {
                "x-auth-token": localStorage.getItem('token')
            }
        })
            .then(res => res.json())
        .then(data => setTransactions(data))
    })
    const updateStatus = (statusId, transactionId) => {
        let body = {
            statusId
        }
        fetch(`${BASE_URL}/transactions/${transactionId}`, {
            method: "PUT",
            body: JSON.stringify(body),
            headers: {
                "Content-Type": "application/json",
                "x-auth-token": localStorage.getItem('token')
            }
        })
            .then(res => res.json())
            .then(data => {
            alert(data.message)
        })
    }
    
    const validateDate = (createdDate) => {        
        let getStartDate = new Date(createdDate).getTime(); 
        let getEndDate = new Date().getTime();
        let differenceInTime = (getEndDate - getStartDate);
        let differenceInDay = Math.ceil(differenceInTime / (1000 * 3600 * 24));
        return (differenceInDay < 3) ? true : false;
    }
    return (
        <div className="container">
           
            <h1 className="cyanText text-center">My Transactions </h1> 
            {
                transactions.length ? 
            <table className="table table-hover">
                <thead className="thead-dark">
                        <tr>
                            <th>Transaction ID</th>
                            <th>Status</th>
                            <th>Amount</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>   
                </thead>
                        <tbody>
                            {transactions.map(transaction => (
                                <tr key={transaction._id}>
                                    <td>{transaction._id}</td>
                                        <td>
                                        {transaction.statusId == "5e9eb2e60cf7a34fb863b113" ?
                                            "Reserved" : (transaction.statusId == "5e9eb2eb0cf7a34fb863b114" ?
                                            "Paid" : "Cancelled"
                                            )
                                         }
                                    </td>
                                    <td>PHP : {transaction.total}</td>
                                    <td>{new Date(transaction.dateCreated).toLocaleString()}</td>
                                    {transaction.statusId === "5e9eb2e60cf7a34fb863b113" ?
                                        <td>
                                            {validateDate(new Date(transaction.dateCreated).toLocaleString()) === false ?
                                                <button className="cancelButton btn disabled">Cancel Your Booking</button>
                                                :
                                                <button 
                                                data-toggle="tooltip" 
                                                title="Will become disable after 3 days"
                                                data-placement="left"
                                                className="cyanButton btn" data-html="true"
                                                onClick={() =>
                                                    updateStatus("5e9eb2f00cf7a34fb863b115", transaction._id)
                                                
                                                }
                                                >
                                                    Cancel Your Booking
                                                </button>
                                               
                                            }
                                        </td> : null}
                                </tr>
                                    ))}
                        </tbody>
            </table>
                        : <h2 className="cyanText text-center"> No transactions to show. </h2>}
                <div className="col-sm-12 col-md-12">
                {
                transactions.length ? 
            <table className="table table-responsive">
                <thead className="thead-dark">
                        <tr>
                            <th>Transaction ID</th>
                            <th>Status</th>
                            <th>Amount</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>   
                </thead>
                        <tbody>
                            {transactions.map(transaction => (
                                <tr key={transaction._id}>
                                    <td>{transaction._id}</td>
                                        <td>
                                        {transaction.statusId == "5e9eb2e60cf7a34fb863b113" ?
                                            "Reserved" : (transaction.statusId == "5e9eb2eb0cf7a34fb863b114" ?
                                            "Paid" : "Cancelled"
                                            )
                                         }
                                    </td>
                                    <td>PHP : {transaction.total}</td>
                                    <td>{new Date(transaction.dateCreated).toLocaleString()}</td>
                                    {transaction.statusId === "5e9eb2e60cf7a34fb863b113" ?
                                        <td>
                                            {validateDate(new Date(transaction.dateCreated).toLocaleString()) === false ?
                                                <button className="cancelButton btn disabled">Cancel Your Booking</button>
                                                :
                                                <button 
                                                data-toggle="tooltip" 
                                                title="Will become disable after 3 days"
                                                data-placement="left"
                                                className="cyanButton btn" data-html="true"
                                                onClick={() =>
                                                    updateStatus("5e9eb2f00cf7a34fb863b115", transaction._id)
                                                
                                                }
                                                >
                                                    Cancel Your Booking
                                                </button>
                                               
                                            }
                                        </td> : null}
                                </tr>
                                    ))}
                        </tbody>
            </table>
            :null}
            </div>
            <p style={italic}>This Transaction table was created by <a href="https://www.facebook.com/oreomeo25" target="_blank">Romeo Marquez</a></p>
        </div>
    )
}

export default UserTransaction;
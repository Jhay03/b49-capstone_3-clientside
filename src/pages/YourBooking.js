import React, {useState, useEffect, Fragment} from 'react';
import Swal from "sweetalert2";
import Stripe from '../components/forms/Stripe';
import error403 from '../images/403.png'
import { BASE_URL } from '../config'
const italic = {
    "fontStyle": "italic",
    "color": "gray"
}
const CheckOut = () => {
    const [bookItems, setBookItems] = useState([])
    let total = 0;
    useEffect(() => {
        setBookItems(JSON.parse(localStorage.getItem('bookItems')))
    }, [])

    if (bookItems.length) {
        let subTotals = bookItems.map(item => parseInt(item.price) * parseInt(item.noOfDays))
        //array or prices
    
         total = subTotals.reduce((accumulator, subPerItem) => {
            return accumulator + subPerItem
        })
    }

    const emptyHandler = () => {
        localStorage.removeItem("bookItems")
        let bookItems = []
        localStorage.setItem("bookItems", JSON.stringify(bookItems))
        window.location.href="/your-booking"
    }

    const deleteBookItem = (roomId) => {
        let updatedBook = bookItems.filter(item => item._id !== roomId)
        localStorage.setItem('bookItems', JSON.stringify(updatedBook))
        setBookItems(JSON.parse(localStorage.getItem('bookItems')))
    }

    
    const checkOut = () => {
        let orders = JSON.parse(localStorage.getItem('bookItems'))
        fetch(`${BASE_URL}/transactions`, {
            method: "POST",
            body: JSON.stringify({orders, total}),
            headers: {
                "Content-Type": "application/json",
                "x-auth-token": localStorage.getItem('token')
            }
        })
            .then(res => res.json())
            .then(data => {
            //console.log(data)
                Swal.fire({
                    'icon': 'success',
                    'title': data.message
                })
                window.location.href=`/booking-details`
                localStorage.setItem('bookItems', JSON.stringify([]))
                setBookItems(JSON.parse(localStorage.getItem('bookItems')))
        })
    }

    const [transactions, setTransactions] = useState([])
    useEffect(() => {
fetch(`${BASE_URL}/transactions`)
        .then(res => res.json())
            .then(data => {
                setTransactions(data)
            })
    }, [])

    return (
        <div className="col-md-10 mx-auto container">
            <h2 className="cyanText text-center"> Booking Details </h2>
            {
                bookItems.length ?
                    <table className="table table-hover">
                        <thead className="thead-dark">
                            <tr>
                                <th>Booking ID</th>
                                <th>Room Name</th>
                                <th>Date Check In</th>
                                <th>Date Check Out</th>
                                <th>Number of Day</th>
                                <th>Price/Night</th>
                               {/*  <th>No. Of Guest</th> */}
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {bookItems.map(item => (
                                <tr key={item._id}>
                                    <td>{item.bookingId}</td>
                                    <td>{item.roomname}</td>
                                    <td>{new Date(item.checkIn).toLocaleString()}</td> 
                                    <td>{new Date(item.checkOut).toLocaleString()}</td> 
                                    <td>{item.noOfDays}</td>
                                    <td>{item.price}</td>
                                    {/* <td>{item.noOfGuest}</td> */}
                                    <td>
                                       {/*<button className="btn btn-outline-danger mr-2" onClick={deletePerItem}>Delete</button> */ } 
                                        <button className="declineMore btn mr-2"
                                            onClick={() => deleteBookItem(item._id)}>
                                            Delete</button>
                                    </td>
                                </tr>
                            ))}
                            <tr>
                                <td colSpan="5">Total</td>
                                <td colSpan="5">PHP : {total}</td>
                            </tr>
                            <tr>
                                <td colSpan="8" className="text-center">
                                    <button className="declineMore btn mr-2" onClick={emptyHandler}>Empty Cart</button>
                                    <button className="cyanButton btn mr-2" onClick={() => checkOut(bookItems._id)}>Check Out</button>
                                    <Stripe amount={total * 100} bookItems={bookItems} setBookItems={setBookItems}/>
                                </td>
                            </tr>
                            </tbody>
                    </table>
                    :
                    <h3 className="italicBoldText text-center">No Booking Found</h3>
            }
            <div className="col-sm-12 col-md-12">
                {
                    bookItems.length ?
                        <table className="table table-responsive">
                            <thead className="thead-dark">
                                <tr>
                                    <th>Booking ID</th>
                                    <th>Room Name</th>
                                    <th>Date Check In</th>
                                    <th>Date Check Out</th>
                                    <th>Number of Day</th>
                                    <th>Price/Night</th>
                                    {/*  <th>No. Of Guest</th> */}
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                {bookItems.map(item => (
                                    <tr key={item._id}>
                                        <td>{item.bookingId}</td>
                                        <td>{item.roomname}</td>
                                        <td>{new Date(item.checkIn).toLocaleString()}</td>
                                        <td>{new Date(item.checkOut).toLocaleString()}</td>
                                        <td>{item.noOfDays}</td>
                                        <td>{item.price}</td>
                                        {/* <td>{item.noOfGuest}</td> */}
                                        <td>
                                            <button className="declineMore btn mr-2"
                                                onClick={() => deleteBookItem(item._id)}>
                                                Delete</button>
                                        </td>
                                    </tr>
                                ))}
                                <tr>
                                    <td colSpan="5">Total</td>
                                    <td colSpan="5">PHP : {total}</td>
                                </tr>
                                <tr>
                                    <td colSpan="4">
                                        <button className="declineMore btn mr-2" onClick={emptyHandler}>Empty Cart</button>
                                        <button className="cyanButton btn mr-2" onClick={() => checkOut(bookItems._id)}>Check Out</button>
                                        <Stripe amount={total * 100} bookItems={bookItems} setBookItems={setBookItems} />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        : null}
            </div>
            <p style={italic}>This Booking table was created by <a href="https://www.facebook.com/oreomeo25" target="_blank">Romeo Marquez</a></p>
        </div>
    )
}


export default CheckOut;
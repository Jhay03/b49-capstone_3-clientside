import React, {useState} from 'react';
import activityMain from '../images/Activity/activityMain.jpg'
import activityPic4 from '../images/Activity/activityPic4.jpg'
import activityPic2 from '../images/Activity/activityPic2.jpg'
import activityPic1 from '../images/Activity/activityPic1.jpg'
import activityPic3 from '../images/Activity/activityPic3.jpg'
//import activityPic5 from '../images/Activity/activityPic2.jpg'
import Logo from '../images/Logo04.png'
import '../App.css'
import DatePicker from 'react-datepicker'
import "react-datepicker/dist/react-datepicker.css";
import { Link } from 'react-router-dom'
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import banner1 from '../images/banner00.jpg'
import banner2 from '../images/banner0.jpg'
import banner3 from '../images/banner1.jpg'
import banner4 from '../images/banner2.jpg'
import banner5 from '../images/banner3.jpg'
import banner6 from '../images/banner5.jpg'


const Home = () => {
    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(new Date());
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      autoplay: true,
      slidesToScroll: 1
    }
    return (
        <div className="container">
            <div className="home-container">
                <Slider {...settings}>
                            <div className="slider-1">
                            <img src={banner1} className="img-fluid" id="bannerImg" />
                            </div>
                            <div className="slider-2">
                            <img src={banner2} className="img-fluid" id="bannerImg"/>
                            </div>
                            <div className="slider-3">
                            <img src={banner3} className="img-fluid" id="bannerImg" />
                            </div>
                            <div className="slider-4">
                            <img src={banner4} className="img-fluid" id="bannerImg"/>
                            </div>
                            <div className="slider-5">
                            <img src={banner5} className="img-fluid" id="bannerImg"/>
                            </div>
                            <div className="slider-6">
                            <img src={banner6} className="img-fluid" id="bannerImg"/>
                            </div>
                </Slider>
            </div>
                <div className="row">
                    <div className="col-lg-8">
                        <h3 className="italicBoldText text-center mt-4">Welcome to Ram Resort</h3>
                        <p className="welcomePara">It is our honor and pleasure to have you in our hotel. To know more about our services and amenities, just refer to the booklet found on the desk in your hotel room.</p>
                        <p className="welcomePara">We have a 24-hour reception desk that you can call in case you need anything. Our staff will be more than willing to assist you with your needs. Here at RAM Resort, your comfort and convenience are always our topmost priority.</p>
                    </div>
                    <div className="col-lg-4 booking-card">
                    <div className="cardBooking card mb-5 mt-3 mr-0">
                         
                        <div className="card-body ml-1">
                        <div className="card-title">
                            <h2 className="italicBoldText">Ram Booking</h2>
                        </div>  
                            <label htmlFor="Check In">Check In Date</label>
                            
                            <div className="mb-2">
                                <DatePicker
                                selected={startDate}
                                onChange={date => setStartDate(date)}
                                minDate={new Date()}
                                showDisabledMonthNavigation
                                className="form-control"
                                
                                />
                            </div>    

                            <label htmlFor="Check Out ">Check Out Date</label>
                            <div>
                                <DatePicker
                                    selected={endDate}
                                    onChange={date => setEndDate(date)}
                                    selectsEnd
                                    startDate={startDate}
                                    endDate={endDate}
                                    minDate={startDate}
                                    className="form-control"
                                />  
                            </div>    

                            <Link to="/all-rooms">  <button className="btn cyanButton mt-3">Check Availability</button></Link>
                        </div>
                    </div>
                </div>
            </div>
        <div className="more-container">
                <div className="row">
                    <div className="col-md-3">
                        <img src={Logo} alt="Logo" className="ramLogoWhite img-fluid"/>
                    </div>
                    
                    <div className="col-md-8">
                        <h3 className="ramLogoWhite">Be mesmerized and enjoy the <br/> breathtaking clear azure waters</h3>
                        <p className="whiteText">Lorem ipsum dolor sit, amet consectetur adipisicing elit. <br/> Deserunt at minus ut reiciendis sapiente ullam odit placeat maxime architecto. Modi!</p>
                            <Link to="/about" className="cyanButton btn">Read More</Link>
                    </div>
                </div>
        </div>
    
        <div className="container">
                <div className="row">
                    <div className="actPic col-sm-12 col-md-3">
                       <img src={activityMain} alt="Stand Up paddle boat" id="activityImg" className="img-fluid"/>
                    </div>
                    
                    <div className="col-sm-12 col-md-3 activityText">
                        <h4>Stand up paddle boat</h4>
                        <p className="homePara">
                                Stand up paddle surfing is an offshoot of surfing that originated in Hawaii. Unlike traditional surfing where the rider sits until a wave comes, 
                                stand up paddle boarders stand on their boards and use a paddle to propel themselves through the water.
                        </p>
                    </div>
                    
                    <div className="actPic col-sm-12 col-md-3">
                        <img src={activityPic4} alt="Jetski" id="activityImg"  className="img-fluid"/>
                    </div>
                    
                    <div className="col-sm-12 col-md-3 activityText2">
                        <h4>Jetski</h4>
                        <p className="homePara">
                                Your vacation wouldn't be complete without a fun Boracay jet ski ride! 
                                Feel the adrenaline rush as you speed along the ocean's turquoise water!
                        </p>
                    </div>
                    </div>

                    <div className="row">
                    <div className="actPic col-sm-12 col-md-3 activityText">
                        <h4>Snorkeling</h4>
                        <p className="homePara">
                            Snorkeling (British and Commonwealth English spelling: snorkelling) is the practice of swimming on or through a body of water while equipped with a diving mask, 
                            a shaped breathing tube called a snorkel, and usually swimfins. 
                            In cooler waters, a wetsuit may also be worn.
                        </p>
                    </div>
                    
                    <div className="actPic col-sm-12 col-md-3 ">
                        <img src={activityPic2} alt="Snorkeling"  id="activityImg" className="img-fluid"/>
                    </div>
                    
                    <div className="col-sm-12 col-md-3 activityText2">
                        <h4>Parasailing</h4>
                        <p className="homePara">
                            Parasailing, also known as parascending or parakiting, 
                            is a recreational kiting activity where a person is towed behind a vehicle (usually a boat) 
                            while attached to a specially designed canopy wing that resembles a parachute, 
                            known as a parasail wing.
                        </p>
                    </div>
                    
                    <div className="actPic col-sm-12 col-md-3 ">
                        <img src={activityPic1} alt="Parasailing" id="activityImg"  className="img-fluid"/>
                    </div>
                    </div>

                    <div className="row">
                    <div className="actPic col-sm-12 col-md-3 ">
                        <img src={activityPic2} alt="Snorkeling" id="activityImg"  className="img-fluid"/>
                    </div>
                    
                    <div className="col-sm-12 col-md-3 activityText2">
                         <h4>Snorkeling</h4>
                        <p className="homePara">
                                Snorkeling (British and Commonwealth English spelling: snorkelling) is the practice of swimming on or through a body of water while equipped with a diving mask, 
                                a shaped breathing tube called a snorkel, and usually swimfins. 
                                In cooler waters, a wetsuit may also be worn. 
                        </p>
                    </div>
                    
                    <div className="actPic col-sm-12 col-md-3">
                        <img src={activityPic3} alt="Banana Boat" id="activityImg"  className="img-fluid"/>
                    </div>
                    
                    <div className="col-sm-12 col-md-3 activityText">
                        <h4>Banana Boat</h4>
                        <p className="homePara">
                            Banana boat was a term, a descriptive nickname,
                            given to fast ships also called banana carriers engaged in the banana trade designed to transport easily spoiled bananas rapidly 
                            from tropical growing areas to northern markets that often carried passengers as well as fruit.
                        </p>
                    </div>
                   </div>
        </div>    

        <div className="photography-container">
            <div className="row">
                <div className="col-lg-6 ramVideo-container">
                    <h3 className="italicBoldText"> Ram Video</h3>
                            <div className="video-container"><iframe width="853" height="480"
                                src="https://www.youtube.com/embed/DGIXT7ce3vQ"
                                frameborder="0" allowfullscreen/></div>
                </div>
                
                <div className="col-lg-6 post-container">
                    <h3 className="italicBoldText ml-2"> Ram Post</h3>
                    <div className="row">
                        <img src={activityMain} alt="first" className="thumb img-fluid img-thumbnail"/>
                        <img src={activityPic4} alt="second" className="thumb img-fluid img-thumbnail"/>
                        <img src={activityPic2} alt="third" className="thumb img-fluid img-thumbnail" />
                        <img src={activityPic1} alt="fourth" className="thumb img-fluid img-thumbnail"/>
                        <img src={activityPic3} alt="fifth" className="thumb img-fluid img-thumbnail "/>
                        <img src={activityMain} alt="sixth" className="thumb img-fluid img-thumbnail"/>
                     </div>   
                     <h3 className="italicBoldText mt-3">#RAMResorts</h3>
                </div>
             </div>        
            </div>
                
        </div>
    )
}

export default Home;
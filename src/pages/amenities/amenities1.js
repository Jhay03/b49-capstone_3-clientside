import React from 'react';


const Amenities1 = () => {
    return (
        <div className="amenities-container p-0">
       <h3 className="cyanText">Amenities</h3>
            <div className="freebies-container mt-3">
                <div className="freebies row">
                    <div className="col-md-3">
                        <p className="freebiesPara">Air conditioned</p>
                        <p className="freebiesPara">Towels Provided</p>
                        <p className="freebiesPara">Stairs</p>
                    </div>

                    <div className="col-md-3">
                        <p className="freebiesPara">Balcony</p>
                        <p className="freebiesPara">Queen Bed</p>
                        <p className="freebiesPara">View</p>
                    </div>

                    <div className=" col-md-3">
                        <p className="freebiesPara">Cable/Satellite TV</p>
                        <p className="freebiesPara">Room Service</p>
                    </div>

                    <div className=" col-md-3">
                        <p className="freebiesPara">Daily Room Service</p>
                        <p className="freebiesPara">Room Safe</p>
                    </div>
                </div>
               
            </div>
            <div className="mt-2">
                        <strong>Inclusions</strong>
                       <ul>
                        <li>Living Area</li>
                        <li>Bath Tub</li>
                        <li>1 King Bed</li>
                        <li>Good for 2</li>
                        <li>Breakfast Included</li>
                        <li>Linen: 300 thread count</li>
                        <li>Terrace</li>
                        <li>lirivate Bathroom</li>
                       <li> Cable Television</li>
                       </ul>
                
            </div>
        </div>
    )
}

export default Amenities1;
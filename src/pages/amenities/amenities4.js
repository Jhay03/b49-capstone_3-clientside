import React from 'react';

const Amenities4 = () => {
    return (
        <div className="amenities-container">
            <h3 className="cyanText">Queen Size Bedroom</h3>
            <div className="freebies-container mt-3">
                <div className="freebies row">
                    <div className=" col-md-3">
                        <p className="freebiesPara">Air conditioned</p>
                        <p className="freebiesPara">Towels Provided</p>
                        <p className="freebiesPara">Stairs</p>
                    </div>

                    <div className=" col-md-3">
                        <p className="freebiesPara">Balcony</p>
                        <p className="freebiesPara">Queen Bed</p>
                        <p className="freebiesPara">View</p>
                    </div>

                    <div className=" col-md-3">
                        <p className="freebiesPara">Cable/Satellite TV</p>
                        <p className="freebiesPara">Room Service</p>
                    </div>

                    <div className=" col-md-3">
                        <p className="freebiesPara">Daily Room Service</p>
                        <p className="freebiesPara">Room Safe</p>
                    </div>
                </div>
               
            </div>
            <div className="roomDesc mt-2">
                       <ul>
                            <li>Air-conditioning</li>
                            <li>LAN and Wi-Fi access</li>
                            <li>Satellite/cable TV</li>
                            <li>LED television</li>
                            <li> Pod dock speaker</li>
                            <li>Mini bar</li>
                            <li>Private toilet and bath</li>
                            <li>Shower</li>
                            <li>NDD/ IDD with charge</li>
                            <li>In-room safe</li>
                            <li>We offer rooms for PWD</li>
                            <li>Babysitting services</li>
                            <li>Laundry</li>
                       </ul>
                
            </div>
        </div>
    )
}

export default Amenities4;
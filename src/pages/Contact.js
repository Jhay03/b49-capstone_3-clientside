import React, {useState} from 'react';
import Swal from "sweetalert2"
import {BASE_URL} from '../config'
const ContactUs = () => {
    const [formData, setFormData] = useState({})

    const onChangeHandler = (e) => {
        setFormData({
            ...formData,
            [e.target.name] : e.target.value
            })
    }

    const onSubmit = (e) => {
        e.preventDefault()
        //alert("hello")
        fetch(`${BASE_URL}/contacts`, {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "application/json"
            }
        })
        .then(res => res.json())
            .then(data => {
                if (data.status === 200) {
                    Swal.fire({
                        icon: "success",
                        text: data.message
                    })
                    window.location.href="/"
                } 
        })
    }
    return (
        <div className="card col-md-6 cardDesign">
        <form className="mx-auto col-sm-12" encType="multipart/form-data" onSubmit={onSubmit}>
            <h3 className="whiteText text-center">Drop a message with us!</h3>
            <div className="form-group text-white">
                <label htmlFor="name">Full Name</label>
                <input
                    type="text"
                    className="form-control"
                    name="fullname"
                    id="fullname"
                    required
                    onChange={onChangeHandler}
                />
                <label htmlFor="email">Email</label>
                <input
                    type="email"
                    className="form-control"
                    name="email"
                    id="email"
                    required
                    onChange={onChangeHandler}
                />
                <label htmlFor="message">Message</label>
                <textarea cols="20" rows="5"
                    className="form-control"
                    required
                    name="message"
                    id="message"
                    onChange={onChangeHandler}
                 >
                    
                </textarea>
                <button className="viewMore btn mt-3">Send</button>
            </div>
        </form>
        </div>
    )
}

export default ContactUs;
import React from 'react';
import coupleLogo from '../images/Couple/picMain.jpg'
import famiyLogo from '../images/Family/familyMain.jpg'
import kingSize from '../images/King/kingMain.jpg'
import queenSize from '../images/Queen/queenMain.jpg'
import foodMain from '../images/Food/foodMain.jpg'
import activityMain from '../images/Activity/activityMain.jpg'
import {Link} from 'react-router-dom'
const Gallery = () => {
    return (
        <div className="container">
            <div className="card-deck mt-3">

                <div className="col-md-4 col-sm-12">
                    <div className="card mb-2 p-2">
                        <img src={coupleLogo} alt="first" className="zoom"/>
                    </div>
                    <Link to="/gallery/coupleroom" > <p className="text-center">Couple Room</p></Link>
                </div>
                
                <div className="col-md-4 col-sm-12">
                     <div className="card mb-2 p-2">
                        <img src={famiyLogo} alt="first" className="zoom"/>
                    </div>
                    <Link to="/gallery/familyroom"><p className="text-center"> Family Room </p></Link>
                </div>

                <div className="col-md-4 col-sm-12">
                     <div className="card mb-2 p-2">
                        <img src={kingSize} alt="first" className="zoom"/>
                    </div>
                    <Link to="/gallery/kingroom"><p className="text-center"> King Size Bedroom </p></Link>
                </div>

                <div className="col-md-4 col-sm-12">
                     <div className="card mb-2 p-2">
                        <img src={queenSize} alt="first" className="zoom"/>
                    </div>
                    <Link to="/gallery/queenroom"> <p className="text-center">Queen Size Bedroom</p></Link>
                </div>
                
                <div className="col-md-4 col-sm-12">
                     <div className="card mb-2 p-2">
                        <img src={foodMain} alt="first" className="zoom"/>
                    </div>
                    <Link to="/gallery/foods"><p className="text-center">Foods</p> </Link>
                </div>

                <div className="col-md-4 col-sm-12">
                     <div className="card mb-2 p-2">
                        <img src={activityMain} alt="first" className="zoom"/>
                    </div>
                    <Link to="/gallery/activities"><p className="text-center"> Activities </p></Link>
                </div>
            </div>
        </div>
    )
}

export default Gallery;
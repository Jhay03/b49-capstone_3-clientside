import React from 'react';
import { Link } from 'react-router-dom'
import coupleLogo from '../../images/Couple/picMain.jpg'
import famiyLogo from '../../images/Family/familyMain.jpg'
import kingSize from '../../images/King/kingMain.jpg'
import queenSize from '../../images/Queen/queenMain.jpg'

const AllRooms = () => {
    const para = {
        "size": "medium",
        "textDecoration": "none",
        "color": " #14bffc"
    }
    return (
        <div className="allRooms-container">
            <h2 className="offerText text-center">We offer 4 type of Rooms. Pick what you want!</h2>

            <div className="room-container">

            <div className="card-deck">

                <div className="col-lg-3 col-sm-12">
                    <div className="card mb-2 p-2">
                        <a href="/couple"><img src={coupleLogo} alt="first" className="zoom"/></a>
                    </div>
                    <Link to="/couple"><p className="text-center" style={para}> Couple </p></Link>
                </div>

                <div className="col-lg-3 col-sm-12">
                    <div className="card mb-2 p-2">
                        <a href="/family"><img src={famiyLogo} alt="first" className="zoom"/></a>
                    </div>
                    <Link to="/family"><p className="text-center" style={para}> Family </p></Link>
                </div>

                <div className="col-lg-3 col-sm-12">
                    <div className="card mb-2 p-2">
                        <a href="king"><img src={kingSize} alt="first" className="zoom"/></a>
                    </div>
                    <Link to="/king"><p className="text-center" style={para}> King </p></Link>
                </div>

                <div className="col-lg-3 col-sm-12">
                    <div className="card mb-2 p-2">
                        <a href="/queen"><img src={queenSize} alt="first" className="zoom"/></a>
                    </div>
                    <Link to="/queen"><p className="text-center" style={para}> Queen </p></Link>
                </div>

                </div>
            </div>
        </div>
    )
}

export default AllRooms;
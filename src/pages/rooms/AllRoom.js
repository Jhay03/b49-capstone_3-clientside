import React, {useState, useEffect} from 'react';
import { Link } from 'react-router-dom'

const AllRoom = () => {
    const style = {
        "height": "50vh",
        "width": "100%"
    }
    const para = {
        "size": "medium",
        "textDecoration": "none",
        "color": "black"
    }
    const [categories, setCategories] = useState([])
    useEffect(() => {
        fetch("http://localhost:4000/categories")
        .then(res => res.json())
        .then(data => {
            setCategories(data)
        })
    }, [])
    
    const showCategories = categories.map(category => (
        <div className="col col-lg-3">
            <div className="card p-2 mb-3 h-100">
                <a href={`/categories/${category._id}`}><img src={`http://localhost:4000/${category.image}`}
                    alt="roomImg" className="img-fluid" style={style} /></a>
                <div className="card-body">
                     <Link to={`/categories/${category._id}`}><p className="text-center" style={para}>{category.name}</p></Link>
                 </div>
               
            </div>
        </div>
    ))
    return (
        <div className="container">
            <h3 className="text-center">We offer 4 type of Rooms. Pick what you want!</h3>
            <div className="card-deck">
              
                    
                {showCategories}
            </div>
        </div>    
    )
}

export default AllRoom;
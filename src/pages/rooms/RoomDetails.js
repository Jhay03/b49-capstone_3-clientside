import React, {useState, useEffect, Fragment} from 'react';
import { useParams } from "react-router-dom";
import Swal from 'sweetalert2';
import EditRoom from '../../components/forms/EditRoom'
import DatePicker from 'react-datepicker'
import "react-datepicker/dist/react-datepicker.css";
import {Link} from 'react-router-dom'
import Amenities1 from '../amenities/amenities1';
import Amenities2 from '../amenities/amenities2';
import Amenities3 from '../amenities/amenities3';
import Amenities4 from '../amenities/amenities4';
import {BASE_URL} from '../../config'
const RoomDetails = ({ user, token, rooms }) => {
    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(new Date());
    const [noOfGuest, setNoOfGuest] = useState(0)
    const [bookingId] = useState("RAM-" + Date.now())
    let getStartDate = startDate.getTime()
    let getEndDate = endDate.getTime()
    let differenceInTime = (getEndDate - getStartDate)
    let differenceInDay = Math.ceil(differenceInTime / (1000 * 3600 * 24))
    
    const style = {
        "height": "50vh",
        "width": "100%"
    }
    const para = {
        "textAlign": "justify",
        "fontSize": "15px",
        "fontWeight": "italic"
    }
    let { id } = useParams()
    const [room, setRoom] = useState({})
    const [showEdit, setShowEdit] = useState(false)
    useEffect(() => {
        fetch(`${BASE_URL}/rooms/${id}`)
            .then(res => res.json())
            .then(data => setRoom(data))
    }, [room])

    const deleteHandler = () => {
        Swal.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
          if (result.value) {
            fetch(`${BASE_URL}/rooms/${id}`, {
                method: "DELETE",
                headers: {
                    "x-auth-token": token
                }
            })
            .then(res => res.json())
            .then(data => {
                Swal.fire(
                  'Deleted!',
                  `${data.message}`,
                  'success'
                )
            })
            
          }
        })
            Swal.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#d33',
          cancelButtonColor: '#3085d6',
          confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
          if (result.value) {
            fetch(`${BASE_URL}/rooms/${id}`, {
                method: "DELETE",
                headers: {
                    "x-auth-token": token
                }
            })
            .then(res => res.json())
            .then(data => {
                Swal.fire(
                  'Deleted!',
                  `${data.message}`,
                  'success'
                )
                window.location.href="/all-rooms"
            })
            
          }
        })
    }

    const validateDate = () => {        
        let getStartDate = startDate
        let getEndDate = endDate
        let differenceInTime = (getEndDate - getStartDate);
        let differenceInDay = Math.ceil(differenceInTime / (1000 * 3600 * 24));
        return (differenceInDay === 0) ? true : false;
    }

    const bookHandler = (e, room) => {
  /*       if (startDate === endDate) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Check In must be different on Check Out'
            })
        }  */
            Swal.fire({
                title: 'Are you sure?',
                text: "It will be added to your booking!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Add It!'
            })
            .then((result) => { 
                if (result.value) {
                        let bookItems = JSON.parse(localStorage.getItem('bookItems'))
                        if (bookItems.length) {
                            let matched = bookItems.find(item => {
                                return item._id === room._id
                            })
                   
                            if (matched) {
                                let updatedCart = bookItems.map(item => {
                                    return item._id === room._id ?
                                        {
                                            ...item, noOfGuest: item.noOfGuest += parseInt(e.target.previousElementSibling.value),
                                            checkIn: startDate, checkOut: endDate, bookingId: bookingId, noOfDays: differenceInDay
                                        } :
                                
                                        item
                                })
                      
                                localStorage.setItem('bookItems', JSON.stringify(updatedCart))
                            } else {
                                bookItems.push({ ...room, noOfGuest, checkIn: startDate, checkOut: endDate, bookingId: bookingId, noOfDays: differenceInDay })
                      
                            }
                        } else {
                            bookItems.push({ ...room, noOfGuest, checkIn: startDate, checkOut: endDate, bookingId: bookingId, noOfDays: differenceInDay })
                   
                        }
                        localStorage.setItem('bookItems', JSON.stringify(bookItems))
                        window.location.href = "/your-booking"
                    }
            })
    }

    return (
        <div className="container">
            <h2 className="cyanText text-center">Room Details </h2>
            <div className="card-deck">
                <div className={showEdit ? "col-md-12" : "col-md-4 col-sm-12"} >
                    <div className="card">
                        {showEdit ? <EditRoom setShowEdit={setShowEdit} room={room} /> : 
                            <div>
                            <img src={`${BASE_URL}/${room.image}`} alt="room" style={style}/>
                            <div className="card-body">
                            <h5 className="text-center">{room.roomname}</h5>
                                    <p>Price: {room.price}</p>
                            <div> 
                            <label htmlFor="noOfGuest">Number of Guest</label>
                            {room.categoryId == "5e9e5fc1e4dbb629c8df7c69" ?
                               <input type="number"
                               name="noOfGuest"
                               id="noOfGuest"
                               className="form-control" placeholder="2 persons below"
                               onChange={(e) => setNoOfGuest(parseInt(e.target.value))} 
                               disabled 
                               /> : 
                               (room.categoryId == "5e9e5fe2e4dbb629c8df7c6a" ?
                               <input type="number"
                               name="noOfGuest"
                               id="noOfGuest"
                               className="form-control disabled" placeholder="6 persons below"
                               onChange={(e) => setNoOfGuest(parseInt(e.target.value))} 
                               disabled
                               /> :
                               (room.categoryId == "5e9e5ff0e4dbb629c8df7c6b" ? 
                               <input type="number"
                                name="noOfGuest"
                                id="noOfGuest"
                                className="form-control disabled" placeholder="6 persons below"
                                onChange={(e) => setNoOfGuest(parseInt(e.target.value))} 
                                disabled
                                /> : 
                                <input type="number"
                                name="noOfGuest"
                                id="noOfGuest"
                                className="form-control disabled" placeholder="6 persons below"
                                onChange={(e) => setNoOfGuest(parseInt(e.target.value))} 
                                disabled
                                />
                                ))
                            }
                            
                            </div>
                             <label htmlFor="Check In">Check In Date</label>
                            
                            <div>
                                <DatePicker
                                placeholder=""
                                selected={startDate}
                                onChange={date => setStartDate(date)}
                                minDate={new Date()}
                                showDisabledMonthNavigation
                                className="form-control"
                                />
                            </div>    

                            <label htmlFor="Check Out">Check Out Date</label>
                            <div>
                                <DatePicker
                                    selected={endDate}
                                    onChange={date => setEndDate(date)}
                                    selectsEnd
                                    startDate={startDate}
                                    endDate={endDate}
                                    minDate={startDate}
                                    className="form-control"
                                />  
                            </div>        
                            </div>
                        {user && token && user.isAdmin ? <div className="card-footer">
                            <button className="btn btn-outline-warning mr-2" onClick={() => setShowEdit(true)}> Edit</button>
                            <button className="declineMore btn mr-2" onClick={deleteHandler}>Delete</button>
                            </div> : 
                            null}    
                            </div>}
                        </div>
                </div>
                {showEdit ? null : 
                
                    <div className="col-md-8">
                        <div className="offset-10 mb-2">
                             <Link to="/all-rooms"><button className="declineMore btn">Back</button></Link>
                        </div>
                   <h3 className="cyanText text-center">{room.roomname}</h3>
                   <hr/>
                   <p className=" ml-3">{room.description}</p>
                    <div className="amenitiesPage mt-3">
                        {room.categoryId == "5e9e5fc1e4dbb629c8df7c69" ?
                                <Amenities1/> : (room.categoryId == "5e9e5fe2e4dbb629c8df7c6a" ?
                                <Amenities2/> :(room.categoryId == "5e9e5ff0e4dbb629c8df7c6b" ? <Amenities3/>: <Amenities4/>
                                ))
                            }
                        </div> 
                        {user && token && !user.isAdmin ?
                        <Fragment>
                        {validateDate(room.checkIn === room.checkOut) ? <button className="cancelButton btn">Book Now!</button> :
                           <button className="cyanButton btn" onClick={(e) => {
                            bookHandler(e, room)
                            e.target.previousElementSibling.value = ''
                        }}> Book Now!</button>
                        }
                        </Fragment> : 
                            <div className="auth mt-5"><h3>Please click <Link to="/login">Login</Link> to book this room</h3></div>}
                    </div>}
             
            </div>
            
        </div>
    )
}

export default RoomDetails;
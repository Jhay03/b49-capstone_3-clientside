import React from 'react';
import {Link} from 'react-router-dom'
import {BASE_URL} from '../../config'
const King = ({ rooms }) => {
    const para = {
        "size": "medium",
        "textDecoration": "none",
        "color": "black"
    }
    const showFilteredRooms = rooms.filter(filteredRoom => filteredRoom.categoryId === "5e9e5ff0e4dbb629c8df7c6b").map(room => (
        <div className="col-lg-3 col-sm-12" key={room._id}>
                    <div className="card p-2 h-100 mb-2">
                            <img src={`${BASE_URL}/${room.image}`} alt="roomImg" className="zoom img-fluid"/>
                        <div className="card-body">
                           <Link to={`/rooms/${room._id}`}>
                            <p className="text-center" style={para}>{room.roomname}</p></Link>
                        </div>
                        <div className="card-title">   
                        <h6>Price : {room.price}</h6>
                        </div>  
                        <Link to={`/rooms/${room._id}`}><button className="viewMore btn">View More Details</button>
                        </Link>
                    </div>
                </div>
    ))
    return (
        <div className="container">
            <div className="text-center">
                <h2 className="cyanText">King Room!</h2>
            </div>
            <p>
                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ipsum odio praesentium numquam ullam consectetur vitae repellat, dignissimos suscipit sequi libero?
            </p>
            <div className="card-deck">
            {showFilteredRooms}
            </div>
        </div>
    )
}

export default King;
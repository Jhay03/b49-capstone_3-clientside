import React, {useState, useEffect, Fragment} from 'react';
import Swal from 'sweetalert2'
import { useParams } from 'react-router-dom'
import {BASE_URL} from '../config'

const Message = ({ token }) => {
    let {id} = useParams()
    const [messages, setMessages] = useState([])

    useEffect(() => {
        fetch(`${BASE_URL}/contacts`)
        .then(res=>res.json())
            .then(data => {
             setMessages(data)
        })
    }, [messages])
    const deleteHandler = () => {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
              if (result.value) {
                  fetch(`${BASE_URL}/contacts/${id}`, {
                      method: "DELETE",
                      headers: {
                          "x-auth-token": token
                      }
                  })
                      .then(res => res.json())
                      .then(data => {
                        Swal.fire(
                            'Deleted!',
                            'Your file has been deleted.',
                            'success'
                          )
                          window.location.href="/messages"
                  })
               }
          })
    }

    const onDelete = (messageId) => {
           // alert(messageId)
           Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
            if (result.value) {
        fetch(`${BASE_URL}/contacts/${messageId}`, {
            method: "DELETE",
            headers: {
            "x-auth-token": token,
            }
        })
         .then(res => res.json())
            .then(data => {
                Swal.fire(
                  'Deleted!',
                  `${data.message}`,
                  'success'
                )
            })
            
          }
        })
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
            if (result.value) {
                fetch(`${BASE_URL}/contacts/${messageId}`, {
            method: "DELETE",
            headers: {
            "x-auth-token": token,
            }
        })
         .then(res => res.json())
            .then(data => {
                Swal.fire(
                  'Deleted!',
                  `${data.message}`,
                  'success'
                )
                window.location.href="/messages"
            })
            
          }
        })
    }      
    const showAllMessages = messages.map(message => (
        
                <tbody>
                    <tr key={message._id}>
                        <td>{message.fullname}</td>
                        <td>{message.email}</td>
                        <td>{message.message}</td>
                        <td>
                            <button className="declineMore btn" onClick={() => onDelete(message._id)}>Delete</button>
                        </td>
                    </tr>
                </tbody> 
           
    ))

 
    return (
        <div className="container">
            <h1 className="cyanText text-center">Messages</h1>
            {
            messages.length ?
            <table className="table table-hover">
             <thead className="thead-dark">
                        <tr>
                            <th>Full Name</th>
                            <th>Email</th>
                            <th>Message</th>
                            <th>Action</th>
                        </tr>   
            </thead> 
           {showAllMessages}
           </table>: <h3 className="cyanText text-center mt-3">No Messages to show</h3>
        }
        </div>
    )
}

export default Message;
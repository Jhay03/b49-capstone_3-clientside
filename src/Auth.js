import React from 'react';
import {Link} from 'react-router-dom'
import error403 from './images/403.png'
const Auth = () => {
    return (
        <div className="container text-center">
            <div className="">

            </div>
            <h1 className="error403 text-center"> 403!</h1>
            <img src={error403} alt="403" className="img-fluid"/>
            <h2 className="cyanText">
                You have no access to this page.
            </h2>
        
            
        </div>
    )
}

export default Auth;
import React, {useState} from 'react';
import Swal from "sweetalert2"
import Logo from '../../images/Logo04.png'
import {BASE_URL} from '../../config'
const Login = () => {
    const [formData, setFormData] = useState([])

    const onChangeHandler = (e) => {
        setFormData({
            ...formData,
            [e.target.name] : e.target.value
        })
    }

    const onSubmit = (e) => {
        e.preventDefault()

        fetch(`${BASE_URL}/users/login`, {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type" : "application/json"
            }
        })
                .then(res => res.json())
                .then(data => {
                    if (data.auth) {
                        Swal.fire({
                            icon: "success",
                            title: data.message,
                            showConfirmButton: false,
                            timer: 45000
                        })
                        localStorage.setItem('user', JSON.stringify(data.user))
                        localStorage.setItem('token', data.token)
                        let bookItems = []
                        localStorage.setItem('bookItems', JSON.stringify(bookItems))
                        window.location.href ="/"
                    } else {
                        Swal.fire({
                            icon: "error",
                            title: "Invalid Credentials",
                            showConfirmButton: false,
                            timer: 1500
                        })
                        
                }
            })
       
    }
    return (
        <div className="login-container h-100">
		<div className="d-flex justify-content-center h-100">
			<div className="user_card">
				<div className="d-flex justify-content-center">
					<div className="brand_logo_container">
						<img src={Logo} className="brand_logo mb-3" alt="Logo"/>
						<h3 className="whiteText text-center">RAM Login</h3>
					</div>
				</div>
				<div className="d-flex justify-content-center form_container">
					<form onSubmit={onSubmit} encType="multipart/form-data">
						<div className="input-group mb-3">
							<div className="input-group-append">
								<span className="input-group-text"><i className="fas fa-user"></i></span>
							</div>
                                <input
                                    type="text" name="username"
                                    className="form-control input_user"
                                    placeholder="username"
                                    name="username"
                                    onChange={onChangeHandler}
                                />
						</div>
						<div className="input-group mb-2">
							<div className="input-group-append">
								<span className="input-group-text"><i className="fas fa-key"></i></span>
							</div>
                                <input
                                    type="password"
                                    name="password"
                                    className="form-control input_pass"
                                    placeholder="password"
                                    onChange={onChangeHandler}
                                />
						</div>
							<div className="d-flex justify-content-center mt-3 login_container">
				 	<button name="button" className="btn login_btn">Login</button>
				   </div>
					</form>
				</div>
		
				
			</div>
		</div>
	</div>
    )
}

export default Login;
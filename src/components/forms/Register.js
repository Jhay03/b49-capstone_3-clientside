import React, { useState }from 'react';
import Swal from "sweetalert2";
import {BASE_URL} from '../../config'

const Register = () => {
    const [formData, setFormData] = useState({})

    const onChangeHandler = (e) => {
        setFormData({
            ...formData,
            [e.target.name] : e.target.value
            })
    }

    const onSubmit = (e) => {
        e.preventDefault()
        fetch(`${BASE_URL}/users`, {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(res => res.json())
            .then(data => {
                if (data.status === 200) {
                    Swal.fire({
                        icon: 'success',
                        text: data.message
                    })
                    window.location.href="/"
                } else {
                    Swal.fire({
                        icon: 'error',
                        text: data.message,
                        timer: 1500,
                        showConfirmButton: false
                    })
                }
        })
    }
    return (
        <div className="card col-md-6 cardDesign">
            <form className="container col-sm-12 mx-auto whiteText" onSubmit={onSubmit} >
                    <div className="form-group ">
                        <h1 className="whiteText text-center">Registration Form</h1>
                        <label htmlFor="fullname"> Fullname</label>
                        <input
                            type="text"
                            name="fullname"
                            id="fullname"
                            className="form-control"
                            onChange={onChangeHandler}
                            required
                        />
                    </div>

                    <div className="form-group ">
                        <label htmlFor="username"> Username</label>
                        <input
                            type="text"
                            name="username"
                            id="username"
                            className="form-control"
                            onChange={onChangeHandler}
                            required
                        />
                    </div>

                    <div className="form-group">
                        <label htmlFor="password"> Password</label>
                        <input
                            type="password"
                            name="password"
                            id="password"
                            className="form-control"
                            onChange={onChangeHandler}
                            required
                        />
                    </div>
                    
                    <div className="form-group">
                        <label htmlFor="password2"> Confirm Password</label>
                        <input
                            type="password"
                            name="password2"
                            id="password2"
                            className="form-control"
                            onChange={onChangeHandler}
                            required
                        />
                    </div>
                    
                    <div className="form-group">
                        <label htmlFor="email"> Email</label>
                        <input
                            type="email"
                            name="email"
                            id="email"
                            className="form-control"
                            required
                            onChange={onChangeHandler}
                        />
                    </div>
                    
                    <div className="form-group">
                        <label htmlFor="number"> Number</label>
                        <input
                            type="number"
                            name="number"
                            id="number"
                            className="form-control"
                            onChange={onChangeHandler}
                            required
                        />
                    </div>
                    <button className="viewMore btn mb-4"
                    >Submit</button>
                    </form>

            </div>
    )
}

export default Register;
import React, {useState, useEffect} from 'react';
import Swal from "sweetalert2"
import {BASE_URL} from '../../config'
const AddRoom = () => {
    const [formData, setFormData] = useState({})
    const [categories, setCategories] = useState([])

    useEffect(() => {
        fetch(`${BASE_URL}/categories`, {
            headers: {
                "x-auth-token": localStorage.getItem("token")
            }
        })
            .then(res => res.json())
            .then(data => {
                setCategories(data)
            })
    }, [])
    
    const onChangeHandler = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        })
    }
    const onHandleFile = (e) => {
        setFormData({
            ...formData,
            image : e.target.files[0]
        })
    }

    const onSubmit = (e) => {
        e.preventDefault()
        //alert("Submit")

        const room = new FormData()
        room.append('roomname', formData.roomname)
        room.append('description', formData.description)
        room.append('price', formData.price)
        room.append('categoryId', formData.categoryId)
        room.append('image', formData.image)
        fetch(`${BASE_URL}/rooms`, {
            method: "POST",
            body: room,
            headers: {
                "x-auth-token": localStorage.getItem("token")
            }
        })
        .then(res => res.json())
        .then(data => {
            if (data.status === 200) {
                Swal.fire({
                    icon: "success",
                    text: data.message
                })
                window.location.href="/all-rooms"
            } else {
                Swal.fire({
                icon: "error",
                text: "Invalid inputs"
                })
            }
    })
    }
    return (
        <div className="card cardDesign col-md-6 ">

        <form className=" mx-auto col-sm-12" encType="multipart/form-data" onSubmit={onSubmit}>
            <h1 className="whiteText text-center" >Add a Room </h1>
            
                <div className="form-group text-white">
                    <label htmlFor="roomname"> Room Name </label>
                    <input type="text"
                        name="roomname"
                        id="roomname"
                        className="form-control"
                        onChange={onChangeHandler}
                    />
                </div>

                <div className="form-group text-white">
                    <label htmlFor="description"> Description </label>
                    <input type="text"
                        name="description"
                        id="description"
                        className="form-control"
                        onChange={onChangeHandler}
                    />
                </div>

                <div className="form-group text-white">
                    <label htmlFor="price"> Price </label>
                    <input type="number"
                        min="1"
                        name="price"
                        id="price"
                        className="form-control"
                        onChange={onChangeHandler}
                    />
                </div>

                <div className="form-group text-white">
                    <label htmlFor="image"> Image </label>
                    <input type="file"
                        name="image"
                        id="image"
                        className="form-control"
                        onChange={onHandleFile}
                    />
            </div>
                 <select className="form-control" name="categoryId" onChange={onChangeHandler} >
                    <option disabled selected>Select Room Type</option>
                    {categories.map(category => (
                        <option value={category._id} key={category._id}>{category.name}</option>
                    ))}
                </select>
                <button className="cyanButton btn  mt-2">Add</button>
        </form>
        </div>
        
    )
}

export default AddRoom;
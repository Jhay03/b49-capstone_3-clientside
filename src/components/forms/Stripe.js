import React from 'react';
import StripeCheckout from "react-stripe-checkout"
import { PUBLISHABLE_KEY, BASE_URL } from "../../config"

const Stripe = ({ amount, bookItems, setBookItems }) => {
    const checkout = (token) => {
        let body = {
            token,
            amount,
            bookItems
        }

        fetch(`${BASE_URL}/transactions/stripe`, {
            method: "POST",
            body: JSON.stringify(body),
            headers: {
                "Content-Type": "application/json",
                "x-auth-token": localStorage.getItem('token')
            }
        })
            .then(res => res.json())
            .then(data => {
                localStorage.setItem('bookItems', JSON.stringify([]))
                window.location.href="/booking-details"
        })
    }
       
    return (
        <StripeCheckout
            stripeKey={PUBLISHABLE_KEY}
            label="Card Payments"
            name="RAM Resort"
            description="in RAM we TRUST"
            panelLabel="submit"
            amount={amount}
            billingAddress={false}
            currency="PHP"
            allowRememberMe={false}
            token={checkout}
        />
    )
}

export default Stripe
import React, {useEffect, useState} from 'react';
import Swal from 'sweetalert2';
import { BASE_URL } from '../../config'

const EditRoom = ({setShowEdit, room}) => {
    const [formData, setFormData] = useState(room)
    const [categories, setCategories] = useState([])
    
    useEffect(() => {
        fetch(`${BASE_URL}/categories`, {
            headers: {
                "x-auth-token": localStorage.getItem("token")
            }
        })
        .then(res => res.json())
        .then(data => {
            setCategories(data)
        })
    }, [room])

    const onChangeHandler = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        })
    }
    const handleFile = (e) => {
        setFormData({
            ...formData,
            image: e.target.files[0]
        })
    }

    const onSubmit = (e) => {
        e.preventDefault()
        let updatedRoom = new FormData()
      
        updatedRoom.append('roomname', formData.roomname)
        updatedRoom.append('description', formData.description)
        updatedRoom.append('categoryId', formData.categoryId)
        updatedRoom.append('price', formData.price)
        if (typeof formData.image === 'object') {
            updatedRoom.append('image', formData.image)
        }
        fetch(`${BASE_URL}/rooms/${formData._id}`, {
            method: "PUT",
            body: updatedRoom,
            headers: {
                "x-auth-token": localStorage.getItem("token")
            }
        })
            .then(res => res.json())
            .then(data => {
                if (data.status === 200) {
                    Swal.fire({
                        icon: "success",
                        'text': data.message
                    })
                    setShowEdit(false)
                } 
        })
    }
    return (
        <form className="mx-auto col-sm-8" encType="multipart/form-data" onSubmit={onSubmit} key={room._id}>
            <h1 className="text-center" >Update a Room </h1>
            
            <div className="form-group">
                
                    <label htmlFor="roomname"> Room Name </label>
                    <input type="text"
                        name="roomname"
                        id="roomname"
                        className="form-control"
                        onChange={onChangeHandler}
                        value={formData.roomname}
                    />
                </div>

                <div className="form-group">
                    <label htmlFor="description"> Description </label>
                    <input type="text"
                        name="description"
                        id="description"
                        className="form-control"
                        onChange={onChangeHandler}
                        value={formData.description}
                    />
                </div>

                <div className="form-group">
                    <label htmlFor="price"> Price </label>
                    <input type="number"
                        name="price"
                        id="price"
                        className="form-control"
                        onChange={onChangeHandler}
                        value={formData.price}
                    />
                </div>

                <div className="form-group">
                    <label htmlFor="image"> Image </label>
                <img src={`${BASE_URL}/${room.image}`} className="img-fluid"/>
                    <input type="file"
                        name="image"
                    id="image"
                    className="form-control"
                    onChange={handleFile}
                    />
            </div>
                 <select className="form-control" name="categoryId" onChange={onChangeHandler}>
                {categories.map(category => {
                    return (
                        category._id === formData.categoryId ? 
                            <option value={category._id} selected>{category.name}</option> :
                            <option value={category._id} key={category._id}>{category.name}</option>
                            )
                        })}
            </select> 
            <button className="btn btn-outline-primary mt-2 mr-2" >Update</button>
            <button className="btn btn-outline-secondary mt-2" type="button" onClick={() => setShowEdit(false)}>Cancel</button>
            
           
        </form>
    )
}

export default EditRoom;
import React, {Fragment, useState} from 'react';
import { Link } from 'react-router-dom';
import Logo from './Logo/Logo01.png'
const Navbar = ({ user, token, logoutHandler }) => {

	/*const [collapsed, setCollapsed] = useState(true)*/
	let guestLinks;
	let authLinks;
	if (!user && !token) { 
		guestLinks = (
			<Fragment>
				<li className="nav-item">
					<Link to="/login" className="nav-link text-white">Login</Link>
				</li>
				<li className="nav-item">
					<Link to="/register" className="nav-link text-white">Register</Link>
				</li>
			</Fragment>
		)
	}

	if (token && user) {
		authLinks = (
			<Fragment>
				{user && token && user.isAdmin === false ?
					<Fragment>
					
					<div className="dropdown">
						<button className="btn dropdown-toggle text-white" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							{user.fullname}
						</button>
						<div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
							<Link to="/your-booking" className="nav-link text-dark">Booking</Link>
							<Link to={`/transactions/${user._id}`} className="nav-link text-dark">Transaction</Link>
							<Link to="/" className="nav-link text-dark" onClick={logoutHandler}>Logout</Link>
						</div>
					</div>
					
					</Fragment>
					: null}
				
				{user.isAdmin ? 
					<Fragment>
					<div className="dropdown">
						<button className="btn dropdown-toggle text-white" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							{user.fullname}
						</button>
						<div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
							<Link to="/add-room" className="nav-link text-dark">Add Room</Link>
							<Link to="/transactions" className="nav-link text-dark">Transaction</Link>
							<Link to="/messages" className="nav-link text-dark">Messages</Link>
							<Link to="/" className="nav-link text-dark" onClick={logoutHandler}>Logout</Link>
						</div>
					</div>
					</Fragment> : null}
			</Fragment>
		)
	}
	return (
		<Fragment>
			<nav className="navbar navbar-expand-lg p-0">
				<Link to="/"><img src={Logo} alt="logo" className="logoStyle ml-5 p-0"/></Link>
 		    <button
				className="navbar-toggler navbar-dark mr-3"
				type="button"
				data-toggle="collapse"
				data-target="#navbarNav">
				<span className="navbar-toggler-icon"></span>
			</button>
			<div className="collapse navbar-collapse" id="navbarNav">
				<ul className="navbar-nav ml-auto">
					<li className="nav-item">
						<Link to="/" className="nav-link text-white">
							Home
						</Link>
					</li>
					
					
					<li className="nav-item">
					<Link to="/gallery" className="nav-link text-white">
							Gallery
					</Link>
					</li>

					<li className="nav-item">
					<Link to="/all-rooms" className="nav-link text-white">
							All Rooms
					</Link>
					</li>
					<li className="nav-item">
						<Link to="/contact-us" className="nav-link text-white">
							Contact Us
						</Link>
				
					</li>
						
					<li className="nav-item">
					<Link to="/about" className="nav-link text-white">
							About Us
					</Link>
					</li>

					{authLinks}
					{guestLinks}
				</ul>
			</div>
		
		</nav>
			<div className="straight-navbar">
			</div>
		</Fragment>	
    )
}

export default Navbar;
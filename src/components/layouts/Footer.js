import React from 'react';

const Footer = () => {
    return (
        <div>
            <div className="straight-footer"></div>
            {/* <footer className="footer text-center text-white">
                <h1>Footer</h1>
            </footer> */}
             <footer className="page-footer font-small pt-2">

            <div className="container-fluid text-center text-md-left">

                <div className="row">

                <div className="col-md-6 mt-md-0 mt-3">

                    <h4 className="italicBoldText text-white">Ram Resort</h4>
                    <h5>The art of meeting your highest expectations.</h5>
                    <h5>This Booking System was created by: <a href="#" className="text-white">Romeo Marquez</a></h5>
                    

                </div>

                <hr className="clearfix w-100 d-md-none h3b-3"/>

                <div className="col-md-3 mb-md-0 mb-3">

                    <h5 className="text-uppercase">Projects</h5>

                    <ul className="list-unstyled">
                    <li>
                        <a href="#!" className="text-white">Static Web</a>
                    </li>
                    <li>
                        <a href="#!"  className="text-white">Dynamic Web</a>
                    </li>
                    <li>
                        <a href="#!"  className="text-white">Fullstack</a>
                    </li>
                   
                    </ul>

                </div>

                <div className="col-md-3 mb-md-0 mb-3">

                    <h5 className="text-uppercase">About Us</h5>

                    <ul className="list-unstyled">
                    <li>
                        <a href="#!"  className="text-white">Home</a>
                    </li>
                    <li>
                        <a href="#!"  className="text-white">About Us</a>
                    </li>
                    <li>
                        <a href="#!"  className="text-white">Gallery</a>
                    </li>
                    </ul>

                </div>

                </div>

            </div>

            </footer> 
        </div>    
    )
}
export default Footer;